package de.nurmarvin.discordbot.command;

import de.nurmarvin.discordbot.DiscordBot;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.dv8tion.jda.core.entities.*;

@Getter @AllArgsConstructor
public abstract class Command {

    private String name;
    private String[] aliases;
    private String description;
    private boolean dev;

    public abstract void execute(String[] args, Message msg);

    public void sendAutoDeleteMessage(MessageChannel channel, MessageEmbed message) {
       this.sendAutoDeleteMessage(channel, message, 5);
    }

    public void sendAutoDeleteMessage(MessageChannel channel, MessageEmbed message, long seconds) {
        DiscordBot.getInstance().sendAutoDeleteMessage(channel, message, seconds);
    }
}