package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.DiscordBot;
import de.nurmarvin.discordbot.broadcasts.BroadcastMessage;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.utils.PermissionHelper;
import de.nurmarvin.discordbot.utils.TimeSpan;
import de.nurmarvin.discordbot.utils.UtilMath;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

import java.io.IOException;
import java.util.UUID;

public class AddBroadcastMessageCommand extends Command {

    private String usage;

    public AddBroadcastMessageCommand() {
        super("addbroadcastmessage", new String[0], "Adds a broadcast message", false);

        this.usage = "Usage: " + DiscordBot.getInstance().getConfig().getPrefix() + this.getName() + " <Delay> <Message...>";
    }

    @Override
    public void execute(String[] args, Message msg) {
        EmbedBuilder embedBuilder = DiscordBot.getInstance().getEmbedBase();

        if (!PermissionHelper.isAdmin(msg.getMember())) {
            msg.delete().queue();
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.appendDescription(
                    String.format("You don't seem to have permissions to do this, %s.", msg.getAuthor().getAsMention()))
                                                                     .build());
            return;
        }

        switch (args.length) {
            case 0:
            case 1: {
                this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.appendDescription(usage).build());
                break;
            }
            default: {
                String time = args[0];
                char identifier = time.charAt(time.length() - 1);
                long duration;

                switch (identifier) {
                    case 'd': {
                        duration = TimeSpan.DAY;
                        break;
                    }
                    case 'h': {
                        duration = TimeSpan.HOUR;
                        break;
                    }
                    case 'm': {
                        duration = TimeSpan.MINUTE;
                        break;
                    }
                    case 's': {
                        duration = TimeSpan.SECOND;
                        break;
                    }
                    default: {
                        this.sendAutoDeleteMessage(msg.getChannel(),
                                                   embedBuilder.appendDescription("Please provide a time identifier, like `d(ays), " +
                                                                                  "(h)ours, (m)inutes or (s)econds`.").build());
                        return;
                    }
                }

                String multiplierString = args[0].split(String.valueOf(identifier))[0];
                int multiplier;

                if(!UtilMath.isNumber(multiplierString))
                {
                    this.sendAutoDeleteMessage(msg.getChannel(),
                                               embedBuilder.appendDescription("Please make sure that the provided " +
                                                                              "string in front of the time identifier" +
                                                                              " is a number.").build());
                    return;
                }

                multiplier = Integer.parseInt(multiplierString);

                if(multiplier < 1)
                {
                    this.sendAutoDeleteMessage(msg.getChannel(),
                                               embedBuilder.appendDescription("The delay must be positive.").build());
                    return;
                }

                StringBuilder stringBuilder = new StringBuilder();

                for (int i = 1; i < args.length; i++) {
                    if(i != 1) stringBuilder.append(" ");
                    stringBuilder.append(args[i]);
                }

                BroadcastMessage broadcastMessage = new BroadcastMessage(UUID.randomUUID(), stringBuilder.toString(),
                                                                         msg.getGuild().getIdLong(),
                                                                         msg.getChannel().getIdLong(),
                                                                         multiplier * duration);

                try {
                    DiscordBot.getInstance().getBroadcastManager().registerBroadcastMessage(broadcastMessage);
                    embedBuilder.appendDescription(String.format("Broadcast Message with UUID `%s` and message `%s` was " +
                                                                 "successfully added for channel %s in `%s`.",
                                                                 broadcastMessage.getUuid(),
                                                                 broadcastMessage.getMessage(),
                                                                 msg.getTextChannel().getAsMention(),
                                                                 msg.getGuild().getName()));

                    msg.delete().queue();

                } catch (IOException e) {
                    e.printStackTrace();
                   embedBuilder.appendDescription("Error while saving broadcast message: " + e.getMessage());
                }

                this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build());
            }
        }
    }
}
