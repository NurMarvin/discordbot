package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.DiscordBot;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.utils.*;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;

public class BanCommand extends Command {
    public BanCommand() {
        super("ban", new String[0], "Bans a member", false);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();
        EmbedBuilder embedBuilder = DiscordBot.getInstance().getEmbedBase();

        if (!PermissionHelper.isAdmin(msg.getMember())) {
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.appendDescription(
                    String.format("You don't seem to have permissions to do this, %s.", msg.getAuthor().getAsMention()))
                                                                     .build());
            return;
        }

        if (args.length > 0) {

            long id;

            try {
                id = MentionHelper.getMemberFromMention(msg.getGuild(), args[0]).getUser().getIdLong();
            } catch (InvalidMentionException | NotAMentionException e) {
                e.printStackTrace();
                try {
                    id = Long.parseLong(args[0]);
                    if (DiscordBot.getInstance().getJda().getUserById(id) == null) throw new InvalidIdException();
                } catch (Exception e1) {
                    this.sendAutoDeleteMessage(msg.getChannel(),
                                               embedBuilder
                                                       .appendDescription("Please make sure you actually mention a " +
                                                                          "user that is on the server or give it's id.")
                                                       .build());
                    return;
                }
            }

            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                if (i != 1) stringBuilder.append(" ");
                stringBuilder.append(args[i]);
            }

            Member member = msg.getGuild().getMemberById(id);

            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription(
                                               String.format("Banned %s with reason `%s`", member.getAsMention(),
                                                             stringBuilder.toString())).build());

            member.getUser().openPrivateChannel().queue(channel -> channel
                    .sendMessage(String.format("You have been banned from `%s` with the reason `%s`",
                                               msg.getGuild().getName(), stringBuilder.toString()))
                    .queue(message -> msg.getGuild().getController().ban(channel.getUser().getId(), 0,
                                                                          stringBuilder.toString()).queue()));
        } else {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Please mention the user you want to ban")
                                                   .build());
        }
    }
}