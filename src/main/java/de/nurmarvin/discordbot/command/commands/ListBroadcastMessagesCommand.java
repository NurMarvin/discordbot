package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.DiscordBot;
import de.nurmarvin.discordbot.broadcasts.BroadcastMessage;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.utils.PermissionHelper;
import de.nurmarvin.discordbot.utils.TimeSpan;
import de.nurmarvin.discordbot.utils.UtilMath;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

import java.io.IOException;
import java.util.UUID;

public class ListBroadcastMessagesCommand extends Command {

    public ListBroadcastMessagesCommand() {
        super("listbroadcastmessages", new String[0], "Lists all broadcast messages", false);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();

        EmbedBuilder embedBuilder = DiscordBot.getInstance().getEmbedBase();

        if (!PermissionHelper.isAdmin(msg.getMember())) {
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.appendDescription(
                    String.format("You don't seem to have permissions to do this, %s.", msg.getAuthor().getAsMention()))
                                                                     .build());
            return;
        }

        embedBuilder.setTitle("List of all Broadcast Messages");

        for (BroadcastMessage broadcastMessage : DiscordBot.getInstance().getBroadcastManager().getBroadcastMessages())
            embedBuilder.addField(broadcastMessage.getUuid().toString(), broadcastMessage.getMessage(), false);

        this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build(), 10);
    }
}
