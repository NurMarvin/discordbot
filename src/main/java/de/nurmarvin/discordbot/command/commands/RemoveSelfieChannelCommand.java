package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.DiscordBot;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.utils.InvalidMentionException;
import de.nurmarvin.discordbot.utils.MentionHelper;
import de.nurmarvin.discordbot.utils.NotAMentionException;
import de.nurmarvin.discordbot.utils.PermissionHelper;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

import java.io.IOException;
import java.util.Arrays;

public class RemoveSelfieChannelCommand extends Command {
    public RemoveSelfieChannelCommand() {
        super("removeselfiechannel", new String[0], "Remove a selfie channel", false);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();
        EmbedBuilder embedBuilder = DiscordBot.getInstance().getEmbedBase();

        if (!PermissionHelper.isAdmin(msg.getMember())) {
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.appendDescription(
                    String.format("You don't seem to have permissions to do this, %s.", msg.getAuthor().getAsMention()))
                                                                     .build());
            return;
        }

        if (args.length == 0) {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Please mention the channel as an argument")
                                                   .build());
            return;
        }

        TextChannel textChannel;

        try {
            textChannel = MentionHelper.getChannelFromMention(args[0]);
        } catch (InvalidMentionException | NotAMentionException e) {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Please make sure you actually mention a " +
                                                                      "channel.").build());
            return;
        }

        if(!DiscordBot.getInstance().getConfig().getSelfieChannelIds().contains(textChannel.getIdLong()))
        {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("This channel is not a selfie channel.").build());
            return;
        }

        DiscordBot.getInstance().getConfig().getSelfieChannelIds().remove(textChannel.getIdLong());
        this.sendAutoDeleteMessage(msg.getChannel(),
                                   embedBuilder.appendDescription("Channel is no longer a selfie channel.").build());

        try {
            DiscordBot.getInstance().saveConfig();
        } catch (IOException e) {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Error:" + Arrays.toString(e.getStackTrace())).build());
        }
    }
}
