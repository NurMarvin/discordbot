package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.DiscordBot;
import de.nurmarvin.discordbot.broadcasts.BroadcastMessage;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.utils.PermissionHelper;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

import java.io.IOException;
import java.util.Calendar;
import java.util.UUID;

public class RemoveBroadcastMessageCommand extends Command {

    private String usage;

    public RemoveBroadcastMessageCommand() {
        super("removebroadcastmessage", new String[0], "Removes a broadcast message", false);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();

        EmbedBuilder embedBuilder = DiscordBot.getInstance().getEmbedBase();

        if (!PermissionHelper.isAdmin(msg.getMember())) {
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.appendDescription(
                    String.format("You don't seem to have permissions to do this, %s.", msg.getAuthor().getAsMention()))
                                                                     .build());
            return;
        }

        if(args.length < 1) {
            embedBuilder.appendDescription("Please provide the broadcast message's UUID.");
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build());
            return;
        }

        try {
            UUID uuid = UUID.fromString(args[0]);

            if(DiscordBot.getInstance().getBroadcastManager().getBroadcastMessageByUUID(uuid) != null) {
                DiscordBot.getInstance().getBroadcastManager().unregisterBroadcastMessage(uuid);
                embedBuilder.appendDescription(String.format("Broadcast message with the UUID `%s` has been removed " +
                                                             "successfully.", uuid));
                this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build());
            } else {
                embedBuilder.appendDescription("There was no broadcast message found for the given UUID.");
                this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build());
            }
        }  catch (IllegalArgumentException e) {
            embedBuilder.appendDescription("The given string is not a valid UUID.");
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build());
        } catch (IOException e) {
            e.printStackTrace();
            embedBuilder.appendDescription("Unable to save removal of broadcast message to file: " + e.getMessage());
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build());
        }
    }
}
