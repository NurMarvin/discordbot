package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.DiscordBot;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.utils.NetworkUtil;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Icon;
import net.dv8tion.jda.core.entities.Message;

import java.io.IOException;

public class UpdateAvatarCommand extends Command {
    public UpdateAvatarCommand() {
        super("updateavatar", new String[0], "Updates the avatar of the bot", true);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();

        EmbedBuilder embedBuilder = DiscordBot.getInstance().getEmbedBase();

        if(args.length == 1) {
            updateAvatar(msg, args[0]);
            return;
        } else if(!msg.getAttachments().isEmpty()) {
            updateAvatar(msg, msg.getAttachments().get(0).getUrl());
            return;
        } else {
            embedBuilder.appendDescription("Please pass either an URL as an argument or attach an image.");
        }

        this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build(), 5);
    }

    private void updateAvatar(Message msg, String url) {
        EmbedBuilder embedBuilder = DiscordBot.getInstance().getEmbedBase();
        try {
            byte[] data = NetworkUtil.download(url);
            msg.getJDA().getSelfUser().getManager().setAvatar(Icon.from(data)).queue(
                    success -> {
                        embedBuilder.appendDescription("Avatar updated.");
                        this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build(), 5);
                    },
                    error -> {
                        embedBuilder.appendDescription("Error while updating avatar! `" + error.getMessage() +
                                                       "`");
                        this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build(), 5);
                    });
        } catch (IOException e) {
            e.printStackTrace();
            embedBuilder.appendDescription("Error while updating avatar! `" + e.getLocalizedMessage() + "`");
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build(), 5);
        }
    }
}
