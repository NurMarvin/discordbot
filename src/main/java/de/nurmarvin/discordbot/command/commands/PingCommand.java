package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.DiscordBot;
import de.nurmarvin.discordbot.command.Command;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

public class PingCommand extends Command {
    public PingCommand() {
        super("ping", new String[] {"pong"}, "Shows the ping of the bot", false);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();

        EmbedBuilder embedBuilder = DiscordBot.getInstance().getEmbedBase();

        embedBuilder.setTitle("Ping");
        embedBuilder.setDescription("🏓  |  Ping: Calculating... | Gateway: Calculating...");

        msg.getChannel().sendMessage(embedBuilder.build()).queue(message -> {
            long ping = msg.getCreationTime().until(message.getCreationTime(), ChronoUnit.MILLIS);
            embedBuilder.setDescription("🏓  |  Ping: " + ping + " ms | Gateway: " + msg.getJDA().getPing() + "ms");
            message.editMessage(embedBuilder.build()).queue(m -> m.delete().queueAfter(10, TimeUnit.SECONDS));
        });
    }
}
