package de.nurmarvin.discordbot.command.commands;

import com.vdurmont.emoji.EmojiManager;
import de.nurmarvin.discordbot.DiscordBot;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.urbandictionary.UrbanDictionary;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

import java.io.IOException;
import java.util.ArrayList;

public class UrbanDictionaryCommand extends Command {
    public UrbanDictionaryCommand() {
        super("urban", new String[] { "urbandictionary" }, "Looks something up on the UrbanDictionary", false);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();
        EmbedBuilder embedBuilder = DiscordBot.getInstance().getEmbedBase();
        if(args.length == 0) {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Please name the term you need a definition " +
                                                                      "for.").build());
        } else {
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < args.length; i++) {
                if(i != 0) stringBuilder.append(" ");
                stringBuilder.append(args[i]);
            }

            String term = stringBuilder.toString();

            try {
                ArrayList<UrbanDictionary.Entry> entries = UrbanDictionary.lookUp(stringBuilder.toString());

                if(entries.size() == 0) {
                    this.sendAutoDeleteMessage(msg.getChannel(),
                                               embedBuilder.appendDescription(String.format("Couldn't find a definition " +
                                                                                       "for the term `%s`", term)).build());
                    return;
                }

                UrbanDictionary.Entry entry = entries.get(0);

                embedBuilder.setTitle(String.format("Definition of \"%s\"", term));

                embedBuilder.setAuthor("Author: " + entry.getAuthor());
                embedBuilder.setDescription(entry.getDefinition());
                embedBuilder.addField("Example", entry.getExample(), false);
                embedBuilder.addField("Perma Link", entry.getPermalink(), false);
                embedBuilder.addField("Rating",
                                      String.format("%s %s %s %s", EmojiManager.getForAlias("thumbsup").getUnicode(),
                                                    entry.getThumbs_up(),
                                                    EmojiManager.getForAlias("thumbsdown").getUnicode(),
                                                    entry.getThumbs_down()),
                                      false);

                this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build(), 30);
            } catch (IOException e) {
                this.sendAutoDeleteMessage(msg.getChannel(),
                                           embedBuilder.appendDescription(String.format("An error occurred when trying " +
                                                                                   "look up the term `%s`: %s", term,
                                                                                        e.getMessage())).build());
            }
        }
    }
}
