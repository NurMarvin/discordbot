package de.nurmarvin.discordbot.command.commands;

import com.vdurmont.emoji.EmojiManager;
import de.nurmarvin.discordbot.DiscordBot;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.urbandictionary.UrbanDictionary;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class EightballCommand extends Command {
    private static final String[] ANSWERS =  {"Yes", "No", "Maybe", "Yes, Cutie", "No, Cutie", "Nah", "Yee", "Meh",
                                              "Yeh", "Yay", "Nay", "Unsure", "Sure", "I don't know", "Probably",
                                              "Possible", "Yuh"};

    public EightballCommand() {
        super("eightball", new String[] { "8ball" }, "Ask something and get an answer.", false);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();
        EmbedBuilder embedBuilder = DiscordBot.getInstance().getEmbedBase();
        if(args.length == 0 || !args[args.length - 1].endsWith("?")) {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Please ask a question. Don't forget to put a " +
                                                                      "question mark at the end.").build());
        } else {
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < args.length; i++) {
                if (i != 0) stringBuilder.append(" ");
                stringBuilder.append(args[i]);
            }

            String answer = ANSWERS[ThreadLocalRandom.current().nextInt(ANSWERS.length)];

            embedBuilder.setTitle("Eightball");
            embedBuilder.addField("Asked by: ",  msg.getAuthor().getAsMention(), false);
            embedBuilder.addField("Question: ", stringBuilder.toString(), false);
            embedBuilder.addField("Answer: ", answer, false);

            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build(), 30);
        }
    }
}
