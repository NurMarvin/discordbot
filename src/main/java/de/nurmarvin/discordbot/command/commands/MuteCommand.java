package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.DiscordBot;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.utils.*;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;

public class MuteCommand extends Command {
    public MuteCommand() {
        super("mute", new String[]{"begone"}, "Mutes people with the `BEGONE THOT` role", false);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();

        EmbedBuilder embedBuilder = DiscordBot.getInstance().getEmbedBase();

        if (!PermissionHelper.isAdmin(msg.getMember())) {
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.appendDescription(
                    String.format("You don't seem to have permissions to do this, %s.", msg.getAuthor().getAsMention()))
                                                                     .build());
            return;
        }

        if (args.length > 0) {

            long id;

            try {
                id = MentionHelper.getMemberFromMention(msg.getGuild(), args[0]).getUser().getIdLong();
            } catch (InvalidMentionException | NotAMentionException e) {
                e.printStackTrace();
                try {
                    id = Long.parseLong(args[0]);
                    if (DiscordBot.getInstance().getJda().getUserById(id) == null) throw new InvalidIdException();
                } catch (Exception e1) {
                    this.sendAutoDeleteMessage(msg.getChannel(),
                                               embedBuilder
                                                       .appendDescription("Please make sure you actually mention a " +
                                                                          "user that is on the server or give it's id.")
                                                       .build());
                    return;
                }
            }

            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                if (i != 1) stringBuilder.append(" ");
                stringBuilder.append(args[i]);
            }

            Member member = msg.getGuild().getMemberById(id);

            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription(
                                               String.format("Muted %s with reason `%s`", member.getAsMention(),
                                                             stringBuilder.toString())).build());

            member.getUser().openPrivateChannel().queue(channel -> channel
                    .sendMessage(String.format("You shall begone from `%s` with the reason `%s`",
                                               msg.getGuild().getName(), stringBuilder.toString()))
                    .queue(message -> {
                        //Give BEGONE THOT role
                        msg.getGuild().getController().addSingleRoleToMember(member,
                                                                        msg.getGuild().getRoleById(527540290387705866L)).queue();
                        //Take Users role
                        msg.getGuild().getController().removeSingleRoleFromMember(member,
                                                                        msg.getGuild().getRoleById(511265586546737182L)).queue();
                    }));
        } else {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Please mention the user you want to mute")
                                                   .build());
        }
    }
}
