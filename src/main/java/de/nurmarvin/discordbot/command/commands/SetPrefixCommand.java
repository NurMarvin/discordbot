package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.DiscordBot;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.utils.PermissionHelper;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

import java.io.IOException;
import java.util.Arrays;

public class SetPrefixCommand extends Command {
    public SetPrefixCommand() {
        super("setprefix", new String[0], "Changes the prefix of the bot", false);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();
        EmbedBuilder embedBuilder = DiscordBot.getInstance().getEmbedBase();

        if (!PermissionHelper.isAdmin(msg.getMember())) {
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.appendDescription(
                    String.format("You don't seem to have permissions to do this, %s.", msg.getAuthor().getAsMention()))
                                                                     .build());
            return;
        }

        if(args.length != 0) {
            DiscordBot.getInstance().getConfig().setPrefix(args[0].toLowerCase());

            embedBuilder.appendDescription(String.format("Prefix has been changed to `%s`", args[0].toLowerCase()));

            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build());
        }

        try {
            DiscordBot.getInstance().saveConfig();
        } catch (IOException e) {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Error:" + Arrays.toString(e.getStackTrace())).build());
        }
    }
}
