package de.nurmarvin.discordbot;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;

@Data @AllArgsConstructor
public class Config {
    private String token;
    private ArrayList<Long> adminRoles;
    private ArrayList<Long> modRoles;
    /*private ArrayList<Long> bypassGroups;*/
    private Boolean adminBypass;
    private ArrayList<Long> selfieChannelIds;
    private Long selfiesChatId;
    private String prefix;
}
