package de.nurmarvin.discordbot.utils;

import de.nurmarvin.discordbot.DiscordBot;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;

import java.util.regex.Pattern;

public class MentionHelper {
    private static final String CHANNEL_REGEX = "<#(\\d{18})>";
    private static final String USER_REGEX = "<@!(\\d{18})>";
    private static final String MEMBER_REGEX = "<@(\\d{18})>";

    public static TextChannel getChannelFromMention(String mention) throws InvalidMentionException,
                                                                            NotAMentionException {
        long id;
        if(Pattern.matches(CHANNEL_REGEX, mention)) {
            id = Long.parseLong(mention.substring(2, mention.length() -1));
            if (DiscordBot.getInstance().getJda().getTextChannelById(id) == null) throw new InvalidMentionException();
        }
        else throw new NotAMentionException();

        return DiscordBot.getInstance().getJda().getTextChannelById(id);
    }

    public static User getUserFromMention(String mention) throws InvalidMentionException,
                                                                 NotAMentionException {
        long id;
        if(Pattern.matches(USER_REGEX, mention)) {
            id = Long.parseLong(mention.substring(2, mention.length() -1));
            if (DiscordBot.getInstance().getJda().getUserById(id) == null) throw new InvalidMentionException();
        }
        else throw new NotAMentionException();

        return DiscordBot.getInstance().getJda().getUserById(id);
    }

    public static Member getMemberFromMention(Guild guild, String mention) throws InvalidMentionException,
                                                                                  NotAMentionException {
        long id;
        if(Pattern.matches(MEMBER_REGEX, mention)) {
            id = Long.parseLong(mention.substring(2, mention.length() -1));
            if (guild.getMemberById(id) == null) throw new InvalidMentionException();
        }
        else throw new NotAMentionException();

        return guild.getMemberById(id);
    }
}
