package de.nurmarvin.discordbot.utils;

public class InvalidMentionException extends Exception {
    public InvalidMentionException() {
        super("The mentioned channel doesn't exist");
    }
}
