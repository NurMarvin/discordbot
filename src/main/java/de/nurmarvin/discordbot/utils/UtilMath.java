package de.nurmarvin.discordbot.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Random;

public class UtilMath
{
	public static double trim(int degree, double d) 
	{
		String format = "#.#";
		
		for (int i=1 ; i<degree ; i++)
			format += "#";

		DecimalFormatSymbols symb = new DecimalFormatSymbols(Locale.US);
		DecimalFormat twoDForm = new DecimalFormat(format, symb);
		return Double.valueOf(twoDForm.format(d));
	}

	
	public static Random random = new Random();
	public static int r(int i) 
	{
		return random.nextInt(i);
	}

	public static double rr(double d, boolean bidirectional)
	{
		if (bidirectional)
			return Math.random() * (2 * d) - d;
		
		return Math.random() * d;
	}
	
	public static boolean isNumber(String string)
	{
		try
		{
			Integer.parseInt(string);
			return true;
		}
		catch(Exception exception)
		{
			return false;
		}
	}
}