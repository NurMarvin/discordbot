package de.nurmarvin.discordbot.utils;

public class NotAMentionException extends Exception {
    public NotAMentionException() {
        super("The given string is not a mention");
    }
}
