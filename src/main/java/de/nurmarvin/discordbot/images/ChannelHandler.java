package de.nurmarvin.discordbot.images;

import de.nurmarvin.discordbot.Config;
import de.nurmarvin.discordbot.DiscordBot;
import de.nurmarvin.discordbot.utils.AttachmentHelper;
import de.nurmarvin.discordbot.utils.PermissionHelper;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class ChannelHandler extends ListenerAdapter {
    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {

        if(event.getAuthor().equals(event.getJDA().getSelfUser()))
            return;

        Config config = DiscordBot.getInstance().getConfig();

        if(config.getSelfieChannelIds().contains(event.getChannel().getIdLong())
           && !PermissionHelper.canBypass(event.getMember()) && !AttachmentHelper.hasImageAttached(event.getMessage())) {
            EmbedBuilder embedBuilder = DiscordBot.getInstance().getEmbedBase();

            if(event.getAuthor().isBot() && !event.getJDA().getSelfUser().equals(event.getAuthor()))
            {
                event.getMessage().delete().queue();
                return;
            }

            embedBuilder.appendDescription(String.format("Please only post selfies in here, any conversations about " +
                                                   "selfies, may be posted in %s, %s.",
                                                         event.getGuild().getTextChannelById(config.getSelfiesChatId()).getAsMention(),
                                                         event.getAuthor().getAsMention()));

            DiscordBot.getInstance().sendAutoDeleteMessage(event.getChannel(), embedBuilder.build(), 5);

            event.getMessage().delete().queue();
        }
    }
}
