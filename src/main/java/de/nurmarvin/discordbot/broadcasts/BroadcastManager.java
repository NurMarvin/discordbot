package de.nurmarvin.discordbot.broadcasts;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.reflect.TypeToken;
import de.nurmarvin.discordbot.DiscordBot;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class BroadcastManager {
    private final File broadcastsFile;
    private ArrayList<BroadcastMessage> broadcastMessages;
    private Map<UUID, ScheduledFuture> schedulers;

    public BroadcastManager() throws IOException {
        this.schedulers = Maps.newHashMap();
        this.broadcastsFile = new File("broadcasts.json");
        if (this.broadcastsFile.exists())
            this.load();
        else {
            this.broadcastMessages = Lists.newArrayList();
            this.save();
        }
    }

    public void startSchedulers() {
        for (BroadcastMessage broadcastMessage : broadcastMessages) {
            startScheduler(broadcastMessage);
        }
    }

    public void registerBroadcastMessage(BroadcastMessage broadcastMessage) throws IOException {
        if(this.isRunning(broadcastMessage)) return;

        this.broadcastMessages.add(broadcastMessage);
        this.startScheduler(broadcastMessage);
        this.save();
    }

    public void unregisterBroadcastMessage(UUID uuid) throws IOException {
        BroadcastMessage broadcastMessage = this.getBroadcastMessageByUUID(uuid);
        if(this.isRunning(broadcastMessage)) stopScheduler(broadcastMessage);

        this.broadcastMessages.remove(broadcastMessage);
        this.save();
    }

    public void startScheduler(BroadcastMessage broadcastMessage) {
        if(this.isRunning(broadcastMessage)) {
            this.stopScheduler(broadcastMessage);
        }

        System.out.println(String.format("Starting Broadcast Message %s with initial delay of 5 seconds",
                                         broadcastMessage.getUuid()));

        final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        this.schedulers.put(broadcastMessage.getUuid(), scheduler.scheduleAtFixedRate(broadcastMessage, 1000 * 5,
                                                                                      broadcastMessage.getDelayInMillis(),
                                                                                      TimeUnit.MILLISECONDS));
    }

    public boolean isRunning(BroadcastMessage broadcastMessage) {
        return schedulers.containsKey(broadcastMessage.getUuid());
    }

    public void stopScheduler(BroadcastMessage broadcastMessage) {
        schedulers.get(broadcastMessage.getUuid()).cancel(true);
    }

    public ArrayList<BroadcastMessage> getBroadcastMessages() {
        return broadcastMessages;
    }

    private void load() throws FileNotFoundException {
        this.broadcastMessages = DiscordBot.getInstance().getGson().fromJson(new FileReader(this.broadcastsFile),
                                                                             new TypeToken<ArrayList<BroadcastMessage>>() {}
                                                                                     .getType());
    }

    public BroadcastMessage getBroadcastMessageByUUID(UUID uuid) {
        for (BroadcastMessage broadcastMessage : broadcastMessages)
            if (broadcastMessage.getUuid().equals(uuid)) return broadcastMessage;
        return null;
    }

    private void save() throws IOException {
        FileWriter fileWriter = new FileWriter(this.broadcastsFile);
        fileWriter.write(DiscordBot.getInstance().getGson().toJson(this.broadcastMessages));
        fileWriter.close();
    }
}
