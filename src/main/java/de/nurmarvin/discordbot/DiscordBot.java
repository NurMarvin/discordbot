package de.nurmarvin.discordbot;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.nurmarvin.discordbot.broadcasts.BroadcastManager;
import de.nurmarvin.discordbot.command.CommandRegistry;
import de.nurmarvin.discordbot.command.commands.*;
import de.nurmarvin.discordbot.images.ChannelHandler;
import de.nurmarvin.discordbot.messages.MessageHandler;
import lombok.Getter;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.MessageEmbed;
import static spark.Spark.*;

import javax.security.auth.login.LoginException;
import java.io.*;
import java.util.concurrent.TimeUnit;

@Getter
public class DiscordBot {
    @Getter
    private static DiscordBot instance;
    private Config config;
    private final Gson gson;
    private final File configFile;
    private JDA jda;
    private CommandRegistry commandRegistry;
    private BroadcastManager broadcastManager;

    public static void main(String[] args) throws IOException, LoginException {
        instance = new DiscordBot();
        instance.init();
    }

    private DiscordBot() {
        this.gson = new GsonBuilder().setPrettyPrinting().create();
        this.configFile = new File("config.json");
    }

    private void handleWebRequests() {
        port(80);
        get("/api/v1/name", (req, res) -> this.getJda().getSelfUser().getName());

        get("/api/v1/gay/:name", (req, res) -> {
            String name = req.params(":name");

            if(name.equalsIgnoreCase("casper")) return "yes";
            else return "no";
        });
    }

    public void init() throws IOException, LoginException {
        if(this.configFile.exists())
            this.loadConfig();
        else {
            this.config = new Config("token", Lists.newArrayList(), Lists.newArrayList(), true, Lists.newArrayList(),
                                     0L, "d!");
            this.saveConfig();
            System.out.println("Created config. Please insert discord token.");
            System.exit(0);
        }

        this.commandRegistry = new CommandRegistry();

        this.commandRegistry.registerCommand(new AddBotAdminCommand());
        this.commandRegistry.registerCommand(new RemoveBotAdminCommand());
        this.commandRegistry.registerCommand(new AddSelfieChannelCommand());
        this.commandRegistry.registerCommand(new HelpCommand());
        this.commandRegistry.registerCommand(new SetSelfieChatChannelCommand());
        this.commandRegistry.registerCommand(new RemoveSelfieChannelCommand());
        this.commandRegistry.registerCommand(new ToggleAdminBypassCommand());
        this.commandRegistry.registerCommand(new SetPrefixCommand());
        this.commandRegistry.registerCommand(new KickCommand());
        this.commandRegistry.registerCommand(new SetNameCommand());
        this.commandRegistry.registerCommand(new UpdateAvatarCommand());
        this.commandRegistry.registerCommand(new PingCommand());
        this.commandRegistry.registerCommand(new BanCommand());
        this.commandRegistry.registerCommand(new AddBroadcastMessageCommand());
        this.commandRegistry.registerCommand(new ListBroadcastMessagesCommand());
        this.commandRegistry.registerCommand(new RemoveBroadcastMessageCommand());
        this.commandRegistry.registerCommand(new MuteCommand());
        this.commandRegistry.registerCommand(new UrbanDictionaryCommand());
        this.commandRegistry.registerCommand(new EightballCommand());

        this.broadcastManager = new BroadcastManager();
        this.broadcastManager.startSchedulers();

        this.handleWebRequests();

        this.jda =
                new JDABuilder(this.config.getToken())
                        .addEventListener(this.commandRegistry)
                        .addEventListener(new ChannelHandler())
                        .addEventListener(new MessageHandler())
                        .build();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                this.saveConfig();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }));
    }

    public EmbedBuilder getEmbedBase() {
        return new EmbedBuilder().setFooter(this.getJda().getSelfUser().getName() + " Bot by NurMarvin#1337",
                                            "https://cdn.discordapp.com/avatars/199127069240590336/a_9068b8dde4e7140a0f49504910df2a3d.gif");
    }

    public void saveConfig() throws IOException {
        FileWriter fileWriter = new FileWriter(this.configFile);
        fileWriter.write(this.gson.toJson(this.config));
        fileWriter.close();
    }

    public void loadConfig() throws FileNotFoundException {
        this.config = this.gson.fromJson(new FileReader(this.configFile), Config.class);
    }

    public void sendAutoDeleteMessage(MessageChannel channel, MessageEmbed message, long seconds) {
        channel.sendMessage(message).queue(m -> m.delete().queueAfter(seconds, TimeUnit.SECONDS));
    }
}
