package de.nurmarvin.discordbot.messages;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableList;
import de.nurmarvin.discordbot.DiscordBot;
import de.nurmarvin.discordbot.utils.PermissionHelper;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.events.message.guild.GuildMessageUpdateEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.List;

public class MessageHandler extends ListenerAdapter {

    private static final List<String> OFFENSIVE_WORDS = ImmutableList.of("nigger", "niger");

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        checkForOffensiveWords(event.getMessage());
    }

    @Override
    public void onGuildMessageUpdate(GuildMessageUpdateEvent event) {
        checkForOffensiveWords(event.getMessage());
    }

    private void checkForOffensiveWords(Message message) {
        if (message.getAuthor().isBot() && !message.getJDA().getSelfUser().equals(message.getAuthor())) {
            return;
        }

        if (message.getAuthor().equals(message.getJDA().getSelfUser()))
            return;

        String messageConverted = new String(message.getContentRaw().getBytes(Charsets.ISO_8859_1))
                .toLowerCase()
                .replace(" ", "")
                .replace("-", "")
                .replace(".", "")
                .replace(" ", "")
                .replace("1", "i")
                .replace("3", "e")
                .replace("a", "e")
                .replace("l", "i")
                .replace("|", "i")
                .replace("!", "i")
                .replace("¡", "i");

        for(String word : OFFENSIVE_WORDS) {
            if (messageConverted.contains(word) &&
                !PermissionHelper.canBypass(message.getMember())) {
                EmbedBuilder embedBuilder = DiscordBot.getInstance().getEmbedBase();

                embedBuilder
                        .appendDescription(
                                String.format("The use of the N word with a hard R is strictly prohibited.\n" +
                                              "Please note that multiple offenses will lead to a mute. %s",
                                              message.getAuthor().getAsMention()));

                DiscordBot.getInstance().sendAutoDeleteMessage(message.getChannel(), embedBuilder.build(), 10);

                message.delete().queue();
                return;
            }
        }
    }
}